
dayjs().format();
dayjs.extend(window.dayjs_plugin_localeData);
dayjs.extend(window.dayjs_plugin_weekOfYear);
dayjs().localeData();
dayjs.locale('fr');

const currentDate = document.querySelector("#current-date");
const weekNumber = document.querySelector("#week-number");
const weekDays = document.querySelector("#week-days");
const dateForm = document.querySelector("#date-form");
const errorLabel = document.querySelector("#error-label");
const timerElements = document.getElementsByClassName("timer-element");

const subtractMonth = document.querySelector("#subtract-month");
const addMonth = document.querySelector("#add-month");

// Date entrée dans le formulaire
let dateInput;

// Mois sélectionné dans le calendrier
// Actuel = 0
let monthOffset = 0;

let showCurrentDate = function () {
    currentDate.innerHTML = dayjs().format('D MMMM YYYY HH:mm:ss');
};

let displayTimer = function () {

    // Récupère les différentes div du HTML
    const daysDiv = document.querySelector("#days-timer");
    const hoursDiv = document.querySelector("#hours-timer");
    const minutesDiv = document.querySelector("#minutes-timer");
    const secondsDiv = document.querySelector("#seconds-timer");

    // On calcule la différence entre dateInput et now
    // Sachant que dateInput est forcément plus grand que now
    let daysDiff = dateInput.diff(dayjs(), 'days');
    daysDiv.innerHTML = daysDiff;
    let timeLeft = dayjs().add(daysDiff, 'days');

    let hoursDiff = dateInput.diff(timeLeft, 'hours');
    hoursDiv.innerHTML = hoursDiff.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
    timeLeft = timeLeft.add(hoursDiff, 'hours');

    let minutesDiff = dateInput.diff(timeLeft, 'minutes');
    minutesDiv.innerHTML = minutesDiff.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });
    timeLeft = timeLeft.add(minutesDiff, 'minutes');

    let secondsDiff = dateInput.diff(timeLeft, 'seconds');
    secondsDiv.innerHTML = secondsDiff.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false });

    // Quand le timer atteind 0, on l'arrête
    if (daysDiff == 0 && hoursDiff == 0 && minutesDiff == 0 && secondsDiff == 0) resetTimer();
};

// Remet le timer à zéro
const resetTimer = function () {
    dateInput = undefined;
    Array.from(timerElements).forEach(element => {
        element.children[0].innerHTML = "00";
    })
}

// Fonction appellée quand la date est plus tôt que now
// ou que le format de date n'est pas bon
const displayError = function () {
    errorLabel.style.display = "block";
    errorLabel.animate([
        {
            backgroundColor: "grey"
        }, {
            backgroundColor: "transparent"
        }
    ], {
        duration: 2000
    });
}

// Fonction appellée toutes les secondes
let realTimeDisplay = function () {
    showCurrentDate();
    if (typeof dateInput !== 'undefined') displayTimer();
}

let showStaticDates = function () {
    const startOfWeek = dayjs().startOf('week').add(1, 'day');

    weekNumber.innerHTML = "Semaine " + dayjs().week() + " -- " + dayjs().format('dddd');
    weekDays.innerHTML = "Semaine du " + startOfWeek.format('D/MM/YYYY') + " au " + startOfWeek.add(6, 'day').format('D/MM/YYYY');
};

let submitHandler = function (event) {
    event.preventDefault();
    // Regex pour une date valide
    var dateformat = /^\d{4}[\-](0?[1-9]|1[012])[\-](0?[1-9]|[12][0-9]|3[01])$/;

    let formObject = Object.fromEntries(new FormData(event.target));
    const dateToCheck = dayjs(formObject.date);

    if (!formObject.date.match(dateformat) || dateToCheck.isBefore(dayjs())) {
        displayError();
        resetTimer();
    } else {
        dateInput = dateToCheck;
        realTimeDisplay();
        errorLabel.style.display = "none";
    }

    event.target.reset();
};

/**
 * Permet d'afficher le calendrier du mois sélectionné par les boutons
 * @param {*} monthOffset Valeur du mois à afficher. 0 = mois en cours
 */
const displayCalendar = function (monthOffset) {

    // Mois sélectionné avec les boutons
    const selectedMonth = dayjs().add(monthOffset, 'month');

    // Récupère les différentes parties du calendrier
    const calendarContainer = document.querySelector("#calendar-container");
    const calendarTitle = document.querySelector("#calendar-title");
    const calendarTable = document.querySelector("#calendar-content")

    // Affiche le mois et l'année
    calendarTitle.innerHTML = selectedMonth.format('MMMM YYYY');

    // Premier jour du calendrier à afficher
    let firstDayOfTable = selectedMonth.startOf('month').startOf('week');

    // clear du contenu de la table
    calendarTable.innerHTML = "";

    //la première ligne du tableau = les jours de la semaine
    const firstLine = document.createElement("tr");
    firstLine.innerHTML = "<th>Lun</th> <th>Mar</th> <th>Mer</th> <th>Jeu</th> <th>Ven</th> <th>Sam</th> <th>Dim</th>";
    calendarTable.appendChild(firstLine);

    // Construction du tableau

    // Toujours 6 lignes et 7 colonnes
    for (let i = 0; i < 6; i++) {
        let row = document.createElement("tr");
        for (let j = 0; j < 7; j++) {
            // On crée une nouvelle cellule
            const cell = document.createElement("td");
            // On récupère le jour à afficher
            let cellDay = firstDayOfTable.add(7 * i + j, 'day')
            cell.innerHTML = cellDay.format('D');
            // DayCheck permet de griser les jours qui ne sont pas du mois en cours
            let dayCheck = parseInt(cell.innerHTML) - (7 * i + j);
            // Grise les jours ou la différence entre numéro du jour et index de la table est trop grand
            if (Math.abs(dayCheck) > 22) {
                cell.style.color = "grey";
            } else {
                // On ajoute un listener sur chaque cellule
                cell.addEventListener("click", event => {
                    // On redéfinit dateInput s'il supérieur au jour actuel
                    if (!cellDay.isBefore(dayjs())) {
                        dateInput = cellDay;
                        realTimeDisplay();
                        errorLabel.style.display = "none";
                    } else {
                        resetTimer();
                    }
                    const selectedCell = document.querySelector("#selected-cell");
                    if (selectedCell != null) selectedCell.id = "";
                    event.target.id = "selected-cell";
                })

            }
            // Bleute le jour actuel
            if (monthOffset == 0 && cell.innerHTML == dayjs().format('D')) cell.style.backgroundColor = "dodgerblue";

            row.appendChild(cell);
        }
        calendarTable.appendChild(row);
    }

    calendarContainer.appendChild(calendarTable);
}

window.addEventListener('DOMContentLoaded', event => {
    showCurrentDate();
    setInterval(realTimeDisplay, 1000);
    showStaticDates();

    displayCalendar(monthOffset);

    dateForm.addEventListener("submit", event => submitHandler(event));
    subtractMonth.addEventListener("click", event => {
        monthOffset -= 1;
        displayCalendar(monthOffset);
    });
    addMonth.addEventListener("click", event => {
        monthOffset += 1;
        displayCalendar(monthOffset);
    })
});